package com.example.crystal.siav1.MissFlightOptions;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.crystal.siav1.R;

/**
 * Created by Crystal on 7/10/2016.
 */

public class Booking_2_Fragment extends Fragment {

    View rootview;

    //click next button to open new fragment "Booking_3_Fragment"
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview=inflater.inflate(R.layout.booking_2,container,false);

        return rootview;
    }

    @Nullable
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        Button btn=(Button)getView().findViewById(R.id.BtnStrToPayment);
        btn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                FragmentManager fragmentManager=getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,new Booking_2_Fragment()).commit();
            }
        });
    }
}
