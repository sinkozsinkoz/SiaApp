package com.example.crystal.siav1.Modules.NextFlightFinder;

/**
 * Created by xinchen on 6/10/16.
 */

public class FlightDetails {
    public String datetime;
    public String GMT;

    public FlightDetails(String datetime, String GMT){
        this.datetime = datetime;
        this.GMT = GMT;
    }
}
