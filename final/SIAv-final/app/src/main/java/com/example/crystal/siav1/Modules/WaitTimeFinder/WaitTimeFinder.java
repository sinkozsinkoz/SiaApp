package com.example.crystal.siav1.Modules.WaitTimeFinder;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xinchen on 6/10/16.
 */

public class WaitTimeFinder {
    private static final String WAIT_TIME_URL = "https://waittime-qa.api.aero/waittime/v1/projection/SIN";
    private static final String API_KEY = "8e2cff00ff9c6b3f448294736de5908a";
    private WaitTimeFinderListener listener;
    public int waittime;

    public WaitTimeFinder(WaitTimeFinderListener listener) {
        this.listener = listener;
    }

    public void execute() throws UnsupportedEncodingException{
        new AccessURL().execute(WAIT_TIME_URL);
    }

    private class AccessURL extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String link = params[0];
            try {
                URL url = new URL(link);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("X-apiKey", API_KEY);

                if (conn.getResponseCode() != 200) {
                    throw new IOException(conn.getResponseMessage());
                }

                // Buffer the result into a string
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                System.out.println(builder.toString());
                reader.close();
                conn.disconnect();

                return builder.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                parseJSon(res);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseJSon(String data) throws JSONException{
        if(data == null)
            return;

        List<QueueTime> queueTimes = new ArrayList<>();
        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonProjections = jsonData.getJSONArray("projections");
        for(int i = 0; i<jsonProjections.length();i++){
            JSONObject jsonProjection = jsonProjections.getJSONObject(i);

            QueueTime queueTime = new QueueTime(jsonProjection.getInt("projectedWaitTime"));
            queueTimes.add(queueTime);
        }
        waittime = listener.onWaitTimeFinderSuccess(queueTimes);
    }
}
