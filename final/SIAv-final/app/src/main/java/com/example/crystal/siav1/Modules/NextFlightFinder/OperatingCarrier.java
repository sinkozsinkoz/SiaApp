package com.example.crystal.siav1.Modules.NextFlightFinder;

/**
 * Created by xinchen on 6/10/16.
 */

public class OperatingCarrier {
    public String airline;
    public String airlineCode;
    public String flightNumber;

    public OperatingCarrier(String airline, String airlineCode, String flightNumber){
        this.airline = airline;
        this.airlineCode = airlineCode;
        this.flightNumber = flightNumber;
    }
}
