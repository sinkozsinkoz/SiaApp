//package com.example.crystal.siav1;
//
//import android.app.ProgressDialog;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.Button;
//import android.widget.Toast;
//
//import java.io.UnsupportedEncodingException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Locale;
//import java.util.Objects;
//
//import Modules.DistanceFinderModule.DistanceFinder;
//import Modules.DistanceFinderModule.DistanceFinderListener;
//import Modules.DistanceFinderModule.Row;
//import Modules.GPSTracker;
//import Modules.NextFlightFinder.FlightRecord;
//import Modules.NextFlightFinder.NextFlightFinder;
//import Modules.NextFlightFinder.NextFlightFinderListener;
//import Modules.WaitTimeFinder.QueueTime;
//import Modules.WaitTimeFinder.WaitTimeFinder;
//import Modules.WaitTimeFinder.WaitTimeFinderListener;
//
//public class MainActivityXC extends AppCompatActivity implements DistanceFinderListener,WaitTimeFinderListener,NextFlightFinderListener{
//
//    public int hours;
//    public int mins;
//    public int distance;
//    public Date earliestDate;
//    public String flightDestination;
//    Button btnShowLocation;
//    GPSTracker gps;
//    ProgressDialog progressDialog;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        flightDestination = "hnd";
//        btnShowLocation = (Button) findViewById(R.id.show_location);
//
//        btnShowLocation.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                sendRequest();
//            }
//        });
//    }
//
//    public void sendRequest(){
//        gps = new GPSTracker(MainActivity.this);
//
//        if (gps.canGetLocation()) {
//            double latitude = gps.getLatitude();
//            double longitude = gps.getLongitude();
//
//            Toast.makeText(getApplicationContext(), "Your location is -\nLat:"
//                    + latitude + "\nLong:" + longitude, Toast.LENGTH_SHORT).show();
//
//            try {
//                new DistanceFinder(this,latitude,longitude).execute();
//                new WaitTimeFinder(this).execute();
//                new NextFlightFinder(this, flightDestination).execute();
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//
//        } else{
//            gps.showSettingsAlert();
//        }
//
//    }
//
//    public void onDistanceFinderStart(){
//        progressDialog = ProgressDialog.show(this, "Please wait.",
//                "Calculating Duration!", true);
//    }
//
//    public void onDistanceFinderSuccess(List<Row> rows){
//        progressDialog.dismiss();
//        for(Row row : rows) {
//            distance = (row.distance.value)/1000;
//            hours = row.duration.hours;
//            mins = row.duration.mins;
//            System.out.println(distance);
//            Toast.makeText(getApplicationContext(), "You are " + distance + " km away from Changi Airport.\n " +
//                    "You will take approximately " + hours + "h" + mins + " min to reach the Airport by Car.", Toast.LENGTH_LONG).show();
//        }
//    }
//
//    public void onWaitTimeFinderSuccess(List<QueueTime> Time){
//        for(QueueTime time : Time){
//            int queuetime;
//            queuetime = (time.queueTime)/60;
//            Toast.makeText(getApplicationContext(), "The queuetime at Changi Airport is currently " + queuetime +" mins", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    public void onNextFlightFinderSuccess(List<FlightRecord> flightRecords){
//        String date = "9999-12-12 23:59:59";
//        try {
//            earliestDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        for(FlightRecord flightRecord: flightRecords) {
//            try {
//                Date current = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(flightRecord.flightDetails.datetime);
//                if(earliestDate.compareTo(current)>0){
//                    earliestDate = current;
//                }
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        }
//        String EarliestDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(earliestDate);
//
//        System.out.println(EarliestDate + "hihi");
//        for(FlightRecord flightRecord: flightRecords){
//            System.out.println(flightRecord.flightDetails.datetime);
//            if(Objects.equals(EarliestDate,flightRecord.flightDetails.datetime)){
//                System.out.println("inside");
//                Toast.makeText(getApplicationContext(), "The earliest flight will be on " + EarliestDate + ". The flight details are as follows:\n" +
//                        "Airline:" + flightRecord.operatingCarrier.airline +"\n"+
//                        "Flight Number: " + flightRecord.operatingCarrier.airlineCode + flightRecord.operatingCarrier.flightNumber +"\n" +
//                        "Date and Time: " + EarliestDate +"\n" +
//                        "Duration: " + flightRecord.duration.duration + " mins\n" +
//                        "Terminal: " + flightRecord.terminal.terminal + "\n", Toast.LENGTH_LONG).show();
//            }
//        }
//    }
//}
