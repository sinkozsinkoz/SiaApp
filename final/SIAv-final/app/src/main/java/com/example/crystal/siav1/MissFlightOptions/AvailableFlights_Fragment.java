package com.example.crystal.siav1.MissFlightOptions;

import android.app.ProgressDialog;
import android.icu.text.DateFormat;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.crystal.siav1.Modules.DistanceFinderModule.DistanceFinder;
import com.example.crystal.siav1.Modules.GPSTracker;
import com.example.crystal.siav1.Modules.NextFlightFinder.FlightRecord;
import com.example.crystal.siav1.Modules.NextFlightFinder.NextFlightFinder;
import com.example.crystal.siav1.Modules.NextFlightFinder.NextFlightFinderListener;
import com.example.crystal.siav1.Modules.WaitTimeFinder.WaitTimeFinder;
import com.example.crystal.siav1.R;

import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * Created by Crystal on 7/10/2016.
 */

public class AvailableFlights_Fragment extends Fragment implements NextFlightFinderListener{
    View rootview;
    public int hours;
    public int mins;
    public int timetoairport = (60*hours)+mins;
    public int timetoflight;
    public int queuetime;
    public int distance;
    public Date earliestDate;

    public String flightDestination="lhr";
    Button btnShowLocation;
    GPSTracker gps;
    ProgressDialog progressDialog;

    //search results global
    String EarliestDate;
    String EarliestAirline;
    String EarliestFlightNumber;
    int EarliestDuration;
    int EarliestTerminal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.booking_nextavailableflights, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState){
        //first flight
        TextView firstCurrentLocText = (TextView)getView().findViewById(R.id.firstCurrentLocText);
        TextView firstDestinationText = (TextView)getView().findViewById(R.id.firstDestinationText);
        TextView firstDateText = (TextView)getView().findViewById(R.id.firstDateText);
        TextView firstFlightNoText = (TextView)getView().findViewById(R.id.firstFlightNoText);
        TextView firstTerminalText = (TextView)getView().findViewById(R.id.firstTerminalText);

//        //second flight
        TextView CurrentLocText = (TextView)getView().findViewById(R.id.secondCurrentLocText);
        TextView secondDestinationText = (TextView)getView().findViewById(R.id.secondDestinationText);
        TextView secondDateText = (TextView)getView().findViewById(R.id.secondDateText);
        TextView secondTimeText = (TextView)getView().findViewById(R.id.secondTimeText);

        sendRequest();


        firstCurrentLocText.setText("From: Singapore");
        firstDestinationText.setText("to London");
        firstDateText.setText("Date/Time: " + String.valueOf(EarliestDate));
        firstFlightNoText.setText("Flight No.: " + String.valueOf(EarliestFlightNumber));
        firstTerminalText.setText("Terminal of Departure: "+EarliestTerminal);



    }

// insert on view created
    //instantiatte the exelements

    public void sendRequest(){
        gps = new GPSTracker(getActivity().getApplicationContext());

        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            Toast.makeText(getActivity().getApplicationContext(), "Your location is -\nLat:"
                    + latitude + "\nLong:" + longitude, Toast.LENGTH_SHORT).show();

            try {
                new NextFlightFinder(this, flightDestination).execute();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else{
            gps.showSettingsAlert();
        }

    }

    public void onNextFlightFinderSuccess(List<FlightRecord> flightRecords){
        String date = "9999-12-12 23:59:59";
        try {
            earliestDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for(FlightRecord flightRecord: flightRecords) {
            try {
                Date current = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(flightRecord.flightDetails.datetime);
                if(earliestDate.compareTo(current)>0){
                    earliestDate = current;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        EarliestDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(earliestDate);

        System.out.println(EarliestDate + "hihi");
        for(FlightRecord flightRecord: flightRecords){
            System.out.println(flightRecord.flightDetails.datetime);
            if(Objects.equals(EarliestDate,flightRecord.flightDetails.datetime)){
                System.out.println("inside");
                EarliestAirline=flightRecord.operatingCarrier.airline;
                EarliestFlightNumber=flightRecord.operatingCarrier.airlineCode;
                EarliestDuration=flightRecord.duration.duration;
                EarliestTerminal=flightRecord.terminal.terminal;

                Toast.makeText(getActivity().getApplicationContext(), "The earliest flight will be on " + EarliestDate + ". The flight details are as follows:\n" +
                        "Airline:" + flightRecord.operatingCarrier.airline +"\n"+
                        "Flight Number: " + flightRecord.operatingCarrier.airlineCode + flightRecord.operatingCarrier.flightNumber +"\n" +
                        "Date and Time: " + EarliestDate +"\n" +
                        "Duration: " + flightRecord.duration.duration + " mins\n" +
                        "Terminal: " + flightRecord.terminal.terminal + "\n", Toast.LENGTH_LONG).show();
            }
        }
    }
}
