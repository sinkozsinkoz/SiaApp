package com.example.crystal.siav1.Modules.NextFlightFinder;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xinchen on 6/10/16.
 */

public class NextFlightFinder {
    private final static String NEXT_FLIGHT_URL = "https://flifo-qa.api.aero/flifo/v3/flights/sin/sia/d/?futureWindow=48&departureOrArrivalAirport=";
    private final static String API_KEY = "2cfd0827f82ceaccae7882938b4b1627";
    private String nextFlightDestination;
    private NextFlightFinderListener listener;

    public NextFlightFinder(NextFlightFinderListener listener, String nextFlightDestination){
        this.listener = listener;
        this.nextFlightDestination = nextFlightDestination;
    }

    public void execute() throws UnsupportedEncodingException{
        new AccessURL().execute(createUrl());
    }

    public String createUrl(){
        String url = NEXT_FLIGHT_URL;
        url += nextFlightDestination;
        return url;
    }

    private class AccessURL extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            String link = params[0];
            try{
                URL url = new URL(link);
                System.out.print(link);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                System.out.print("afterconnect");
                conn.setRequestMethod("GET");
                conn.setRequestProperty("X-apiKey", API_KEY);

                if (conn.getResponseCode() != 200) {
                    throw new IOException(conn.getResponseMessage());
                }

                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder builder = new StringBuilder();
                String line;
                while((line = reader.readLine()) != null){
                    builder.append(line);
                }

                System.out.println(builder.toString());
                reader.close();
                conn.disconnect();

                System.out.println(builder.toString());
                return builder.toString();

            }catch (MalformedURLException e) {
                e.printStackTrace();
            }catch(IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res){
            try{
                parseJSon(res);
            } catch(JSONException e){
                e.printStackTrace();
            }
        }
    }

    private void parseJSon(String data) throws JSONException{
        List<FlightRecord> flightRecords = new ArrayList<>();
        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonFlightRecords = jsonData.getJSONArray("flightRecord");
        for(int i = 0; i<jsonFlightRecords.length(); i++){
            FlightRecord flightRecord = new FlightRecord();
            JSONObject jsonFlightRecord = jsonFlightRecords.getJSONObject(i);
            JSONObject jsonOperatingCarrier = jsonFlightRecord.getJSONObject("operatingCarrier");


            String[] splits = jsonFlightRecord.getString("scheduled").split("T|\\+");
            String split = splits[0] + " " + splits[1];

            System.out.println(split);

            flightRecord.duration = new Duration(jsonFlightRecord.getInt("duration"));
            flightRecord.terminal = new Terminal(jsonFlightRecord.getInt("terminal"));
            flightRecord.status = new Status(jsonFlightRecord.getString("status"));
            flightRecord.flightDetails = new FlightDetails(split ,splits[2]);
            flightRecord.operatingCarrier = new OperatingCarrier(jsonOperatingCarrier.getString("airline"),
                    jsonOperatingCarrier.getString("airlineCode"), jsonOperatingCarrier.getString("flightNumber"));

            flightRecords.add(flightRecord);
        }
        listener.onNextFlightFinderSuccess(flightRecords);
    }
}
