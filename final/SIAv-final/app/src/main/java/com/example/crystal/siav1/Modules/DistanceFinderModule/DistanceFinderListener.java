package com.example.crystal.siav1.Modules.DistanceFinderModule;

import java.util.List;

/**
 * Created by xinchen on 4/10/16.
 */

public interface DistanceFinderListener {
    int onDistanceFinderSuccess(List<Row> rows);
}
