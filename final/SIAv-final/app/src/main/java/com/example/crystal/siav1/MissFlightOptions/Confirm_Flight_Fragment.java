package com.example.crystal.siav1.MissFlightOptions;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.crystal.siav1.Home_Fragment;
import com.example.crystal.siav1.R;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

/**
 * Created by Crystal on 7/10/2016.
 */

public class Confirm_Flight_Fragment extends Fragment {

    View rootview;

    //click next button to open new fragment "Booking_2_Fragment"
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview=inflater.inflate(R.layout.booking_confirmationpage,container,false);

        return rootview;
    }

    @Nullable
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt(); // Here!
            throw new RuntimeException(ex);
        }
        //Button btn=(Button)getView().findViewById(R.id.BtnNext);
        FragmentManager fragmentManager=getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame,new Home_Fragment()).commit();

    }
}
