package com.example.crystal.siav1.Modules.WaitTimeFinder;

import java.util.List;

/**
 * Created by xinchen on 6/10/16.
 */

public interface WaitTimeFinderListener {
    int onWaitTimeFinderSuccess(List<QueueTime> queueTime);
}
