package com.example.crystal.siav1;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class MyPastTrips_Fragment extends Fragment{

    View rootview;

    //click past trip button to go to new activity showing past trips
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview=inflater.inflate(R.layout.mypasttrips_layout,container,false);

        return rootview;
    }

    public void onViewCreated(final View view, Bundle savedInstanceState){
        Button btn=(Button)getView().findViewById(R.id.btnUpcomingTrips);
        btn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                FragmentManager fragmentManager=getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,new MyTrips_Fragment()).commit();
            }
        });
    }



}