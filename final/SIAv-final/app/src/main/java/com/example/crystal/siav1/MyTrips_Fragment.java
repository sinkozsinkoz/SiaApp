package com.example.crystal.siav1;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Crystal on 5/10/2016.
 */

public class MyTrips_Fragment extends Fragment{

    View rootview;

    //click past trip button to go to new activity showing past trips
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview=inflater.inflate(R.layout.mytrips_layout,container,false);

        return rootview;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState){
        Button btn=(Button)getView().findViewById(R.id.btnPastTrips);
        btn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                FragmentManager fragmentManager=getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,new MyPastTrips_Fragment()).commit();
                //Intent intent = new Intent(view.getContext(), MyPastTripsActivity.class);
                //startActivity(intent);
            }
        });

        TextView DestinationText=(TextView)getView().findViewById(R.id.MyTripUpcomingFlightDestinationText);
        TextView DateText=(TextView)getView().findViewById(R.id.MyTripUpcomingFlightDateText);
        TextView TimeText=(TextView)getView().findViewById(R.id.MyTripUpcomingFlightTimeText);
        //get upcoming flight in global variable, set TextView

        DestinationText.setText("London");
        DateText.setText("13 Nov 2016");
        TimeText.setText("10:45am - 4:15pm");
    }

}
