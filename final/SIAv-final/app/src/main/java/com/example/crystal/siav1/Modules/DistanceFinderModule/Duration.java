package com.example.crystal.siav1.Modules.DistanceFinderModule;

/**
 * Created by xinchen on 6/10/16.
 */

public class Duration {
    public String text;
    public int value;

    public int hours;
    public int mins;
    public int totalmins;

    public Duration(String text, int value){
        this.text = text;
        this.value = value;
        this.hours = value/3600;
        this.mins = value%60;
        this.totalmins = value/60;
    }
}
