package com.example.crystal.siav1;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.example.crystal.siav1.MissFlightOptions.AvailableFlights_Fragment;
import com.example.crystal.siav1.Modules.DistanceFinderModule.DistanceFinder;
import com.example.crystal.siav1.Modules.DistanceFinderModule.DistanceFinderListener;
import com.example.crystal.siav1.Modules.DistanceFinderModule.Row;
import com.example.crystal.siav1.Modules.GPSTracker;
import com.example.crystal.siav1.Modules.NextFlightFinder.FlightRecord;
import com.example.crystal.siav1.Modules.NextFlightFinder.NextFlightFinder;
import com.example.crystal.siav1.Modules.NextFlightFinder.NextFlightFinderListener;
import com.example.crystal.siav1.Modules.WaitTimeFinder.QueueTime;
import com.example.crystal.siav1.Modules.WaitTimeFinder.WaitTimeFinder;
import com.example.crystal.siav1.Modules.WaitTimeFinder.WaitTimeFinderListener;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class Navigation_Drawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,DistanceFinderListener,WaitTimeFinderListener,NextFlightFinderListener {

    public int hours;
    public int mins;
    public int finalhours;
    public int queueTime;
    public int timetoairport = (60*hours)+mins;
    public int timetoflight;
    public int queuetime;
    public int distance;
    public Date earliestDate;
    public String flightDestination = "lhr";
    Button btnShowLocation;
    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation__drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setLogo(R.drawable.logo);


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(R.drawable.navicon);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fragmentManager=getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame,new Home_Fragment(),"hometag").commit();
        System.out.println("1");
        sendRequest();


        System.out.println("hours: "+queuetime);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation__drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        //update main content by replacing fragments
        int id = item.getItemId();
        FragmentManager fragmentManager=getSupportFragmentManager();

        if (id == R.id.nav_home) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame,new Home_Fragment()).commit();
            // Handle the camera action
        } else if (id == R.id.nav_mytrips) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame,new MyTrips_Fragment()).commit();
        } else if (id == R.id.nav_boardingpass) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame,new BoardingPass_Fragment()).commit();
        } else if (id == R.id.nav_searchflights) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame,new AvailableFlights_Fragment()).commit();

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void sendRequest(){
        System.out.println("2");
        gps = new GPSTracker(Navigation_Drawer.this);
        System.out.println("3");
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            Toast.makeText(getApplicationContext(), "Your location is -\nLat:"
                    + latitude + "\nLong:" + longitude, Toast.LENGTH_SHORT).show();
            System.out.println("4");
            try {
                System.out.println("beforeexecute");
                DistanceFinder distfind = new DistanceFinder(this,latitude,longitude);
                distfind.execute();
                finalhours = distfind.hours;
                System.out.println("dist");
                WaitTimeFinder waitTimeFinder = new WaitTimeFinder(this);
                waitTimeFinder.execute();
                queueTime = waitTimeFinder.waittime;
                System.out.println("wait");
                new NextFlightFinder(this, flightDestination).execute();
                System.out.println("nextfligh");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else{
            gps.showSettingsAlert();
        }

    }

    public int onDistanceFinderSuccess(List<Row> rows){
        for(Row row : rows) {
            distance = (row.distance.value)/1000;
            hours = row.duration.hours;
            mins = row.duration.mins;
            System.out.println(distance);
            System.out.println("hours in fn" + hours);
            Toast.makeText(getApplicationContext(), "You are " + distance + " km away from Changi Airport.\n " +
                    "You will take approximately " + mins + " min to reach the Airport by Car.", Toast.LENGTH_LONG).show();
        }
        try {
            hours = rows.get(0).duration.hours;
        }catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }
        return hours;
    }

    public int onWaitTimeFinderSuccess(List<QueueTime> Time){
        for(QueueTime time : Time){
            queuetime = (time.queueTime)/60;
            Toast.makeText(getApplicationContext(), "The queuetime at Changi Airport is currently " + queuetime +" mins", Toast.LENGTH_SHORT).show();
        }
        queuetime = Time.get(0).queueTime;
        return queuetime;
    }

    public void onNextFlightFinderSuccess(List<FlightRecord> flightRecords){
        String date = "9999-12-12 23:59:59";
        try {
            earliestDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for(FlightRecord flightRecord: flightRecords) {
            try {
                Date current = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(flightRecord.flightDetails.datetime);
                if(earliestDate.compareTo(current)>0){
                    earliestDate = current;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        String EarliestDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(earliestDate);

        System.out.println(EarliestDate + "hihi");
        for(FlightRecord flightRecord: flightRecords){
            System.out.println(flightRecord.flightDetails.datetime);
            if(Objects.equals(EarliestDate,flightRecord.flightDetails.datetime)){
                System.out.println("inside");
                Toast.makeText(getApplicationContext(), "The earliest flight will be on " + EarliestDate + ". The flight details are as follows:\n" +
                        "Airline:" + flightRecord.operatingCarrier.airline +"\n"+
                        "Flight Number: " + flightRecord.operatingCarrier.airlineCode + flightRecord.operatingCarrier.flightNumber +"\n" +
                        "Date and Time: " + EarliestDate +"\n" +
                        "Duration: " + flightRecord.duration.duration + " mins\n" +
                        "Terminal: " + flightRecord.terminal.terminal + "\n", Toast.LENGTH_LONG).show();
            }
        }
    }

    public int getqueuetime(){
        return queueTime;
    }

    public int getfinalhours() {
        return finalhours;
    }
}


