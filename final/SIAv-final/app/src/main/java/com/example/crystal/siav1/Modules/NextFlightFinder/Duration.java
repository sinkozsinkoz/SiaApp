package com.example.crystal.siav1.Modules.NextFlightFinder;

/**
 * Created by xinchen on 6/10/16.
 */

public class Duration {
    public int duration;

    public int hours;
    public int mins;

    public Duration(int duration){
        this.duration = duration;
        this.hours = duration/60;
        this.mins = duration%60;
    }
}
