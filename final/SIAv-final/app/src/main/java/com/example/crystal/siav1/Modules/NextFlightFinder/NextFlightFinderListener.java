package com.example.crystal.siav1.Modules.NextFlightFinder;

import java.util.List;

/**
 * Created by xinchen on 6/10/16.
 */

public interface NextFlightFinderListener {
    void onNextFlightFinderSuccess(List<FlightRecord> flightRecords);
}
