package com.example.crystal.siav1;


import android.support.v4.app.FragmentManager;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import com.example.crystal.siav1.MissFlightOptions.AvailableFlights_Fragment;
import com.example.crystal.siav1.Modules.DistanceFinderModule.DistanceFinder;
import com.example.crystal.siav1.Modules.DistanceFinderModule.DistanceFinderListener;
import com.example.crystal.siav1.Modules.DistanceFinderModule.Row;
import com.example.crystal.siav1.Modules.GPSTracker;
import com.example.crystal.siav1.Modules.NextFlightFinder.FlightRecord;
import com.example.crystal.siav1.Modules.NextFlightFinder.NextFlightFinder;
import com.example.crystal.siav1.Modules.NextFlightFinder.NextFlightFinderListener;
import com.example.crystal.siav1.Modules.WaitTimeFinder.QueueTime;
import com.example.crystal.siav1.Modules.WaitTimeFinder.WaitTimeFinder;
import com.example.crystal.siav1.Modules.WaitTimeFinder.WaitTimeFinderListener;

import static android.support.v7.appcompat.R.id.time;
import static android.view.View.GONE;

/**
 * Created by Crystal on 5/10/2016.
 */

public class Home_Fragment extends Fragment{
    View rootview;
    ProgressIndicator mProgressIndicator1;
    float max = 1;
    float update = 0;
    boolean threadRunning = false;

    public int hours;
    public int mins;
    public int timetoairport = (60*hours)+mins;
    public int timetoflight;
    public int queuetime;
    public int distance;
    public Date earliestDate;
    public String flightDestination="lhr";
    Button btnShowLocation;
    GPSTracker gps;
    ProgressDialog progressDialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.home_layout, container, false);
        return rootview;
    }

    @Nullable
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        final TextView countdownTimer = (TextView)getView().findViewById(R.id.CountdownTimer);
        mProgressIndicator1 = (ProgressIndicator) getView().findViewById(R.id.determinate_progress_indicator1);
        TextView UpcomingDestination = (TextView) getView().findViewById(R.id.UpcomingFlightDestinationText);
        TextView UpcomingDate = (TextView) getView().findViewById(R.id.UpcomingFlightDateText);
        TextView UpcomingTime = (TextView) getView().findViewById(R.id.UpcomingFlightTimeText);
        TextView TimeLeftText = (TextView)getView().findViewById(R.id.TimeLeftText);
        TextView Reminder = (TextView)getView().findViewById(R.id.TimeLeftText);
        mProgressIndicator1.setForegroundColor(Color.parseColor("#2a36b1"));
        mProgressIndicator1.setBackgroundColor(Color.parseColor("#afbfff"));
        mProgressIndicator1.setVisibility(GONE);
        countdownTimer.setVisibility(GONE);

        Navigation_Drawer activity = (Navigation_Drawer) getActivity();
        int timetoairport = activity.getfinalhours();
        int hourstoairport=timetoairport/60;
        int minstoairport=timetoairport%60;
        int waitingtime=activity.getqueuetime();
//
//
//


        TimeLeftText.setText("From your location, it will take "+Integer.toString(hourstoairport)+" hours and "+Integer.toString(minstoairport)+" minutes"+
        " to get to the airport by car. Estimated airport waiting queue time is "+Integer.toString(waitingtime)+" minutes.");

        UpcomingDestination.setText("London");
        UpcomingDate.setText("8 Oct 2016 - 16 Oct 2016");
        UpcomingTime.setText("12:35 - 19:05");

        int currenthour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int currentminute = Calendar.getInstance().get(Calendar.MINUTE);
        int hourtoflight=12-currenthour;
        int minutetoflight=35-currentminute;

        timetoflight=(hourtoflight*60)+minutetoflight;
        hourtoflight=timetoflight/60;
        minutetoflight=timetoflight%60;

        timetoflight=350;

        //make progress bar and countdown visible, start them, set reminder
            //push notification here
        if(timetoflight<=360) {
            mProgressIndicator1.setVisibility(View.VISIBLE);
            countdownTimer.setVisibility(View.VISIBLE);
            String hourleft=String.valueOf(hourtoflight);
            String minleft=String.valueOf(minutetoflight);
            if(hourtoflight<10){
                hourleft="0"+String.valueOf(hourtoflight);
            }
            if(minutetoflight<10){
                minleft="0"+String.valueOf(hourtoflight);
            }

            countdownTimer.setText(hourleft+":"+minleft);
           startThread();

            Reminder.setText("It is less than 6 hours to your flight, you should leave soon!");
            //see if can add weather at destination airport
        }
            if(timetoairport+queuetime+45>timetoflight) { //5min margin since checkout closes 40 mins in advance
            //miss flight option;
                // button icon becomes visible and new fragment when clicked
                //push notification here
                Reminder.setText("You might not be able to catch your flight. Let us help you get on the next one.");
                FragmentManager fragmentManager=getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,new AvailableFlights_Fragment()).commit();
            }
            if(timetoflight<=45) {
                Reminder.setText("");
            };






        //get current time for clock, progress bar and miss flight option
        //get next flight information and display
        // UpcomingDestination.setText() to display the information
        //start the timer 6 hours before flight
        /*while(true){
            if(flight time-gettime=21600 seconds){
                startthread();
                circle visibility on;
                timer visibility on;
                timer starts;
             }
           }
         */
        /*while (update<=max) {
          update=(flighttime-gettime)/21600;
          updateProgressIndicatorValue()
          }
          threadrunning=false;
          visibility=off;}
         */


    }

    @Override
    public void onResume() {
        super.onResume();
        if(threadRunning)
            return;
        else startThread();
    }

    private void startThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                threadRunning = true;
                update = 0;
                while(update <= max-0.1){
                    update = 1-(timetoflight/360);
                    updateProgressIndicatorValue();
                    try{
                        Thread.sleep(100);
                    }catch(Exception e){

                    }
                }
                threadRunning = false;
            }
        }).start();
    }
    @Nullable
    private void updateProgressIndicatorValue() {
        if(this.getActivity()==null)return;
        this.getActivity().runOnUiThread(new Runnable() {
            @Override

            public void run() {
                mProgressIndicator1.setValue(update);
            }
        });
    }



}

