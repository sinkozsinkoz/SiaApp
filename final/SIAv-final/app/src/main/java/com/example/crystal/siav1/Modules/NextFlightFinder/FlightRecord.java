package com.example.crystal.siav1.Modules.NextFlightFinder;

/**
 * Created by xinchen on 6/10/16.
 */

public class FlightRecord {
    public Duration duration;
    public FlightDetails flightDetails;
    public OperatingCarrier operatingCarrier;
    public Status status;
    public Terminal terminal;
}
