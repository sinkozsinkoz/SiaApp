package com.example.crystal.siav1.Modules.DistanceFinderModule;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;



/**
 * Created by xinchen on 4/10/16.
 */

public class DistanceFinder {
    private static final String DISTANCE_FIND_URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&";
    private static final String GOOGLE_API_KEY = "AIzaSyCkRmqr2xtRhpHCUB3xw5o8HFfYWqt2JMM";
    private static final String T2COORDINATES = "1.363710,103.991209";
    private DistanceFinderListener listener;
    private double latitude;
    private double longitute;
    public int hours;


    public DistanceFinder(DistanceFinderListener listener, double latitude, double longitute){
        System.out.println("distfinder");
        this.listener = listener;
        this.latitude = latitude;
        this.longitute = longitute;
    }

    public void execute() throws UnsupportedEncodingException {
        System.out.println("success");
        System.out.println(createUrL(latitude,longitute));
        new DownloadRawData().execute(createUrL(latitude,longitute));
    }

    public String createUrL(double latitude,double longitute) throws UnsupportedEncodingException{
        return DISTANCE_FIND_URL + "origins=" + latitude + "," + longitute + "&destinations=" + T2COORDINATES + "&key=" + GOOGLE_API_KEY;
    }

    private class DownloadRawData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params){
            String link = params[0];
            try{
                URL url = new URL(link);
                InputStream is = url.openConnection().getInputStream();
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                String line;
                while((line = reader.readLine()) != null){
                    buffer.append(line + "\n");
                }
                return buffer.toString();

            }catch (MalformedURLException e) {
                e.printStackTrace();
            }catch(IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res){
            try{
                parseJSon(res);
            } catch(JSONException e){
                e.printStackTrace();
            }
        }
    }

    private void parseJSon(String data) throws JSONException {
        if (data == null)
            return;

        List<Row> rows = new ArrayList<>();
        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonRows = jsonData.getJSONArray("rows");
        for(int i=0; i<jsonRows.length();i++) {
            Row row = new Row();
            JSONObject jsonRow = jsonRows.getJSONObject(i);
            JSONArray jsonElements = jsonRow.getJSONArray("elements");
            JSONObject jsonElement = jsonElements.getJSONObject(0);
            JSONObject jsonDistance = jsonElement.getJSONObject("distance");
            JSONObject jsonDuration = jsonElement.getJSONObject("duration");

            row.distance = new Distance(jsonDistance.getString("text"),jsonDistance.getInt("value"));
            row.duration = new Duration(jsonDuration.getString("text"), jsonDistance.getInt("value"));
            System.out.println(row.distance);
            System.out.println(row.duration);

            rows.add(row);
        }
        hours = listener.onDistanceFinderSuccess(rows);
    }
}
