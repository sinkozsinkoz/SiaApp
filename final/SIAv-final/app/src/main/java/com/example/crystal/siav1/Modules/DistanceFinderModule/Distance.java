package com.example.crystal.siav1.Modules.DistanceFinderModule;

/**
 * Created by xinchen on 6/10/16.
 */

public class Distance {
    public String text;
    public int value;

    public Distance(String text, int value){
        this.text = text;
        this.value = value;
    }
}
